FROM node:12.8-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install and cache apps dependencies
COPY package.json /usr/src/app/package.json
RUN npm install
RUN npm install react-scripts@2.1.8 -g

CMD ["npm", "start"]

