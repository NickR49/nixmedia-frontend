import { observable } from 'mobx';
import Game from '../types/Game';
import Player from '../types/Player';

export interface IStore {
  game: Game;
  player: Player;
}

export default class Store implements IStore {
  @observable game: Game;
  @observable player: Player;

  constructor() {
    this.game = new Game();
    this.player = new Player();
  }
}
