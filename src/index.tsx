import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'mobx-react';
import { ApolloProvider } from 'react-apollo';
import { ApolloProvider as ApolloHookProvider } from '@apollo/react-hooks';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink, Observable } from 'apollo-link';
import { withClientState } from 'apollo-link-state';

// import { resolvers, defaults } from "./resolvers";

import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import App from './App';
import Store from './stores/Store';

// // Apollo link state TypeDefs
// const typeDefs = `
//   type Todo {
//     id: Int!
//     text: String!
//     completed: Boolean!
//   }

//   type Mutation {
//     addTodo(text: String!): Todo
//     toggleTodo(id: Int!): Todo
//   }

//   type Query {
//     visibilityFilter: String
//     todos: [Todo]
//   }
// `;

// Apollo Boost migration - https://www.apollographql.com/docs/react/advanced/boost-migration.html

const cache = new InMemoryCache();

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) => {
      if (message === 'Unauthorized') {
        console.log('errorLink: graphQLError 401');
        localStorage.removeItem('token');
        cache.writeData({
          data: {
            authStatus: {
              __typename: 'authStatus',
              status: 'loggedOut',
            },
          },
        });
      }
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      );
      return null;
    });
  }

  if (networkError) {
    // [Network error]: TypeError: Failed to fetch
    console.log(`[Network error]: ${networkError}`);
    // if (networkError.statusCode === 401) {
    //   console.log("errorLink: networkError 401");
    // }
  }
});

const request = async (operation: any) => {
  const token = await localStorage.getItem('token');
  // const token = await AsyncStorage.getItem('token');
  operation.setContext({
    headers: {
      token: token ? `Bearer ${token}` : '',
    },
  });
};

const requestLink = new ApolloLink(
  (operation, forward) =>
    new Observable(observer => {
      let handle: any;
      Promise.resolve(operation)
        .then(oper => request(oper))
        .then(() => {
          if (forward) {
            handle = forward(operation).subscribe({
              next: observer.next.bind(observer),
              error: observer.error.bind(observer),
              complete: observer.complete.bind(observer),
            });
          }
        })
        .catch(observer.error.bind(observer));

      return () => {
        if (handle) handle.unsubscribe();
      };
    })
);

const stateLink = withClientState({
  cache,
  resolvers: {},
  // set default state, else you'll get errors when trying to access undefined state
  defaults: {
    authStatus: {
      __typename: 'authStatus',
      status: 'loggedOut',
    },
  },
});

const httpLink = new HttpLink({
  uri: `${process.env.REACT_APP_BACKEND_URL}`,
  credentials: 'same-origin',
  // credentials: "include",
});

const client = new ApolloClient({
  link: ApolloLink.from([
    errorLink,
    requestLink,
    // withClientState({ defaults, resolvers, cache }),
    stateLink,
    httpLink,
  ]),
  cache,
});

ReactDOM.render(
  <div className="app-container">
    <ApolloProvider client={client}>
      <ApolloHookProvider client={client}>
        <Provider store={new Store()}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </Provider>
      </ApolloHookProvider>
    </ApolloProvider>
  </div>,

  document.getElementById('app-root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
