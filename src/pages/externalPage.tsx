import React, { Component } from 'react';
import styled from 'styled-components';

import homeImage from '../assets/images/home.jpg';
import CloseButton from '../components/common/CloseButton';

const BackgroundStyles = styled.div`
  background-image: url(${homeImage});
  filter: blur(8px);
  -webkit-filter: blur(8px);
  height: 1000px;
  /* height: 100%; */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

const TextStyles = styled.div`
  display: grid;
  background-color: rgb(0, 0, 0); /* Fallback color */
  background-color: rgba(0, 0, 0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 20px;
  text-align: center;
`;

interface ExternalPageProps {
  onClose?: any;
}

class ExternalPage extends Component<ExternalPageProps> {
  state = {};
  render() {
    return (
      <div className="home-container">
        <BackgroundStyles />
        <TextStyles>
          {this.props.onClose && <CloseButton onClose={this.props.onClose} />}
          {this.props.children}
        </TextStyles>
      </div>
    );
  }
}

export default ExternalPage;
