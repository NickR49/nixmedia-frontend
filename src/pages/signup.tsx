import React, { Component } from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import produce from 'immer';

// import Form from "./styles/Form";
import Error from '../components/common/errorMessage';
import { CURRENT_USER_QUERY } from '../components/user';
import SignupForm from '../components/signup-form';
import ExternalPage from './externalPage';

const SIGNUP_MUTATION = gql`
  mutation SIGNUP_MUTATION(
    $email: String!
    $password: String!
    $name: String!
  ) {
    signup(email: $email, password: $password, name: $name) {
      token
      user {
        id
        email
        name
      }
    }
  }
`;

interface SignupProps {
  history: any;
}

class Signup extends Component<SignupProps> {
  state = {
    email: '',
    password: '',
    name: '',
  };

  async componentDidMount() {
    // Ensure any existing token is removed
    await localStorage.removeItem('token');
  }

  saveToState = (e: any) => {
    // old
    // this.setState({ [e.target.name]: e.target.value });

    // new
    this.setState(
      produce(draft => {
        draft[e.target.name] = e.target.value;
      })
    );
  };

  handleClose = () => {
    this.props.history.push('/');
  };

  render() {
    return (
      <ExternalPage onClose={this.handleClose}>
        <ApolloConsumer>
          {client => (
            <Mutation
              mutation={SIGNUP_MUTATION}
              variables={this.state}
              onCompleted={async ({ signup: payload }: { signup: any }) => {
                // Could potentially receive token in headers, however for now will be explicitly passed token in response
                await localStorage.setItem('token', payload.token);
                // Get fresh 'me' data for Nav
                await client
                  .query({
                    query: CURRENT_USER_QUERY,
                    fetchPolicy: 'network-only',
                  })
                  .then(() => {
                    this.props.history.push('/play');
                  });
              }}
            >
              {(signup: any, result: any) => {
                const { error, loading } = result;
                if (loading) return <p>Loading ...</p>;
                return (
                  <>
                    <SignupForm query={signup} />
                    <Error error={error} />
                  </>
                );
              }}
            </Mutation>
          )}
        </ApolloConsumer>
      </ExternalPage>
    );
  }
}

export default Signup;
