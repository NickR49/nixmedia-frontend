import React, { Component } from 'react';
import styled from 'styled-components';
import ExternalPage from './externalPage';

const HomeGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
`;

interface HomeProps {
  history: any;
}

class Home extends Component<HomeProps> {
  state = {};
  render() {
    return (
      <ExternalPage>
        <HomeGrid>
          <h1>Nix Media</h1>
          <br />
          <div style={{ display: 'inline' }}>
            <button
              onClick={() => this.props.history.push('/login')}
              className="btn btn-success"
            >
              Login
            </button>
            &nbsp;
            <button
              onClick={() => this.props.history.push('/signup')}
              className="btn btn-success"
            >
              Signup
            </button>
          </div>
        </HomeGrid>
      </ExternalPage>
    );
  }
}

export default Home;
