import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import { Mutation, ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
// import Form from "./styles/Form";
import Error from '../components/common/errorMessage';
import { CURRENT_USER_QUERY } from '../components/user';
import LoginForm from '../components/login-form';
import ExternalPage from './externalPage';

const LOGIN_MUTATION = gql`
  mutation LOGIN_MUTATION($email: String!, $password: String!) {
    signin(email: $email, password: $password) {
      token
      user {
        id
        email
        name
      }
    }
  }
`;

interface LoginProps extends RouteComponentProps<any> {
  history: any;
}

class Login extends Component<LoginProps> {
  state = {
    email: '',
    password: '',
  };

  async componentDidMount() {
    // Ensure any existing token is removed
    await localStorage.removeItem('token');
  }

  // saveToState = (e: any) => {
  //   this.setState({ [e.target.name]: e.target.value });
  // };

  handleClose = () => {
    this.props.history.push('/');
  };

  render() {
    return (
      <ExternalPage onClose={this.handleClose}>
        <ApolloConsumer>
          {client => (
            <Mutation
              mutation={LOGIN_MUTATION}
              variables={this.state}
              onCompleted={async ({ signin: payload }: { signin: any }) => {
                // Could potentially receive token in headers, however for now will be explicitly passed token in response
                await localStorage.setItem('token', payload.token);
                // Get fresh 'me' data for Nav
                await client
                  .query({
                    query: CURRENT_USER_QUERY,
                    fetchPolicy: 'network-only',
                  })
                  .then(() => {
                    // Store basic user info - Note: the User component already exposes 'me' data
                    // client.writeData({ data: { user: result.data.me } });
                    // client.writeData({ data: { playername: result.data.me.name } });
                    client.writeData({
                      data: {
                        authStatus: {
                          __typename: 'authStatus',
                          status: 'loggedIn',
                        },
                      },
                    });
                  });
                // this.props.history.push('/play');
                this.props.history.push('/movies');
              }}
            >
              {(signin: any, results: any) => {
                const { error, loading } = results;
                if (loading) return <p>Loading ...</p>;
                return (
                  <>
                    <LoginForm signin={signin} />
                    <Error error={error} />
                  </>
                );
              }}
            </Mutation>
          )}
        </ApolloConsumer>
      </ExternalPage>
    );
  }
}

export default withRouter(Login);
