import React, { Component } from 'react';

import Modal from '../components/common/modal';
import Logout from '../components/logout';
// import { CURRENT_USER_QUERY } from '../components/user';
import CloseButton from '../components/common/CloseButton';
import ToWatchFC from '../components/toWatch';
import MoviePlayer from '../components/moviePlayer';

interface IProps {}

interface IState {
  showModal: boolean;
  url: string;
}

class Movies extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      showModal: false,
      url: '',
    };
  }

  handleShow = () => {
    this.setState({ showModal: true });
  };

  handleHide = () => {
    this.setState({ showModal: false });
  };

  render() {
    const modal = this.state.showModal ? (
      <Modal>
        <div className="modal">
          <div className="modal-content">
            <CloseButton onClose={() => this.handleHide()} />
            <MoviePlayer url={this.state.url} />
          </div>
        </div>
      </Modal>
    ) : null;

    return (
      <>
        <div className="logout">
          <Logout />
        </div>

        {modal}
        <ToWatchFC
          onSelectMovie={(url: string) =>
            this.setState({ url, showModal: true })
          }
        />
      </>
    );
  }
}

export default Movies;
