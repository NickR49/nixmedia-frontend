import React, { Component } from 'react';
// import Form from "./styles/Form";

interface LoginFormProps {
  signin: any;
  history?: any;
}

class LoginForm extends Component<LoginFormProps> {
  state = {
    email: '',
    password: '',
  };

  saveToState = (e: any) => {
    if (e === null || e.target === null) return;
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e: any) => {
    e.preventDefault();
    this.props.signin({ variables: this.state });
  };

  render() {
    return (
      <form
        method="post"
        onSubmit={this.onSubmit}
        onLoad={() => {
          const token = localStorage.getItem('token');
          // if (token) this.props.history.push('/play');
          if (token) this.props.history.push('/movies');
        }}
      >
        <fieldset>
          <h2>Login</h2>
          <label htmlFor="email">
            Email
            <input
              type="email"
              name="email"
              placeholder="email"
              value={this.state.email}
              onChange={this.saveToState}
            />
          </label>
          <br />
          <label htmlFor="password">
            Password
            <input
              type="password"
              name="password"
              placeholder="password"
              value={this.state.password}
              onChange={this.saveToState}
            />
          </label>
          <br />
          <button className="btn btn-success" type="submit">
            Login
          </button>
        </fieldset>
      </form>
    );
  }
}

export default LoginForm;
