import React, { useState } from 'react';
// import Form from "./styles/Form";

interface SignupFormProps {
  query: any;
}

function SignupForm(props: SignupFormProps) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');

  const onSubmit = (e: any) => {
    e.preventDefault();
    props.query({ variables: { email, password, name } });
  };

  return (
    <form method="post" onSubmit={onSubmit}>
      <fieldset>
        <h2>Sign up</h2>
        <label htmlFor="name">
          Name
          <input
            type="text"
            name="name"
            placeholder="name"
            value={name}
            onChange={e => setName(e.target.value)}
          />
        </label>
        <br />
        <label htmlFor="email">
          Email
          <input
            type="email"
            name="email"
            placeholder="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </label>
        <br />
        <label htmlFor="password">
          Password
          <input
            type="password"
            name="password"
            placeholder="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </label>
        <br />
        <button className="btn btn-success" type="submit">
          Sign Up!
        </button>
      </fieldset>
    </form>
  );
}

export default SignupForm;
