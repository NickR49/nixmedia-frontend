import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import { Mutation, ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import { CURRENT_USER_QUERY } from './user';

const LOG_OUT_MUTATION = gql`
  mutation LOG_OUT_MUTATION {
    signout {
      message
    }
  }
`;

interface LogoutProps extends RouteComponentProps<any> {
  // history: any;
}

class Logout extends Component<LogoutProps> {
  render() {
    return (
      <ApolloConsumer>
        {client => (
          <Mutation
            mutation={LOG_OUT_MUTATION}
            onCompleted={async (signout: any) => {
              // Remove token
              await localStorage.removeItem('token');
              // Get fresh 'me' data for Nav
              await client
                .query({
                  query: CURRENT_USER_QUERY,
                  fetchPolicy: 'network-only',
                })
                .then(result => {
                  this.props.history.push('/');
                });
            }}
          >
            {(signout: any) => (
              <button
                className="btn btn-success"
                onClick={async () => {
                  await signout();
                }}
              >
                Sign Out
              </button>
            )}
          </Mutation>
        )}
      </ApolloConsumer>
    );
  }
}

export default withRouter(Logout);
