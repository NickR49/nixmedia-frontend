import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';

const CURRENT_USER_QUERY = gql`
  query {
    me {
      id
      email
      name
      permissions
    }
  }
`;

const UserQuery = (props: any) => (
  <Query {...props} query={CURRENT_USER_QUERY}>
    {(payload: any) => props.children(payload)}
  </Query>
);

UserQuery.propTypes = {
  children: PropTypes.func.isRequired,
};

export default UserQuery;
export { CURRENT_USER_QUERY };
