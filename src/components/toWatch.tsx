import React from 'react';
import gql from 'graphql-tag';
import { useQuery, useMutation } from '@apollo/react-hooks';

import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';

const MOVIES_QUERY = gql`
  query MOVIES_QUERY {
    movies {
      entries {
        id
        title
        description
        contents {
          url
        }
        images {
          url
        }
      }
    }
  }
`;

const WATCH_MOVIE_MUTATION = gql`
  mutation WATCH_MOVIE_MUTATION($id: String!) {
    watchMovie(id: $id) {
      message
    }
  }
`;

const responsive = {
  214: {
    items: 1,
  },
  428: {
    items: 2,
  },
  642: {
    items: 3,
  },
  856: {
    items: 4,
  },
  1070: {
    items: 5,
  },
};

interface ToWatchProps {
  onSelectMovie: any;
}

function ToWatch(props: ToWatchProps) {
  const [watchMovie] = useMutation(WATCH_MOVIE_MUTATION);
  const { loading, error, data: queryData } = useQuery(MOVIES_QUERY);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error</p>;
  // if (data.me === null) return <p>Please login</p>;
  const { movies } = queryData;
  const { entries } = movies;

  const handleOnDragStart = (e: any) => e.preventDefault();

  const galleryItems = entries.map((movie: any) => (
    <div key={movie.id}>
      <img
        onClick={() => {
          watchMovie({ variables: { id: movie.id } });
          props.onSelectMovie(movie.contents[0].url);
        }}
        // onKeyPress={e => {
        //   const code = e.keyCode ? e.keyCode : e.which;
        //   alert('key code: ' + code);
        //   if (code === 13) {
        //     alert('enter press');
        //   }
        // }}
        alt={movie.description}
        src={
          //TODO Set a placeholder image for where image is missing (can do in backend)
          // movie.images[0].url === ''
          //   ? '../assets/images/placeholder.jpg'
          //   :
          movie.images[0].url
        }
        onDragStart={handleOnDragStart}
        style={{ alignSelf: 'center' }}
      />

      <h3
        style={{
          fontSize: 12,
          color: 'black',
          textAlign: 'center',
          width: 214,
        }}
      >
        {movie.title}
      </h3>
    </div>
  ));

  return (
    <>
      <div>
        <span className="section-header">To Watch</span>
      </div>

      <AliceCarousel
        items={galleryItems}
        responsive={responsive}
        fadeOutAnimation={true}
        mouseDragEnabled={true}
        buttonsDisabled={false}
        dotsDisabled={true}
      />
    </>
  );
}

export default ToWatch;
export { MOVIES_QUERY };
