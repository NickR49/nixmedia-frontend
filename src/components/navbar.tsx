import React, { Component } from 'react';
import Logout from './logout';

class NavBar extends Component {
  render() {
    return (
      <>
        <div />
        <p>Nix Media</p>
        <div className="nav-item">
          {/* <Logout history={this.props.history} /> */}
          <Logout />
        </div>
      </>
    );
  }
}

export default NavBar;
