import React from 'react';

interface IProps {
  onClose: any;
}

const CloseButton = (props: IProps) => {
  return (
    <button onClick={props.onClose} className="close">
      &times;
    </button>
  );
};

export default CloseButton;
