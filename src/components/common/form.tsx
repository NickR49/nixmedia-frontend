import styled from 'styled-components';

const Form = styled.form`
  font-size: 1.2rem;
  line-height: 2;
  font-weight: 400;
  label {
    display: block;
    margin-bottom: 0rem;
  }
  input,
  textarea,
  select {
    padding: 0.5rem;
    font-size: 1rem;
    border: 1px solid black;
  }
  button,
  input[type='submit'] {
    border: 0;
    font-size: 1rem;
    font-weight: 400;
    padding: 0.5rem 1.2rem;
  }
`;

export default Form;
