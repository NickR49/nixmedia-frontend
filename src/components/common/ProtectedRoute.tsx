import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import auth from '../../services/authService';

interface IProps extends RouteProps {
  path: string;
  render?: any;
}

const ProtectedRoute = (props: IProps) => {
  const { component: Component, render, ...rest } = props;
  return (
    <Route
      {...rest}
      render={props => {
        if (!auth.getCurrentUser())
          return (
            <Redirect to={{ pathname: '/', state: { from: props.location } }} />
          );
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRoute;
