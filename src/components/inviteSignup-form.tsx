import React, { Component } from 'react';
// import Form from "./styles/Form";

interface InviteSignupFormProps {
  query: any;
  invite: any;
}

class InviteSignupForm extends Component<InviteSignupFormProps> {
  state = {
    email: '',
    password: '',
    name: '',
    inviterName: '',
  };

  constructor(props: InviteSignupFormProps) {
    super(props);
    const { inviteeName, inviteeEmail, inviterId } = props.invite;
    const { name: inviterName } = inviterId;
    this.state = {
      email: inviteeEmail,
      password: '',
      name: inviteeName,
      inviterName,
    };
  }

  saveToState = (e: any) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e: any) => {
    e.preventDefault();
    this.props.query({ variables: this.state });
  };

  render() {
    return (
      <form method="post" onSubmit={this.onSubmit}>
        <h1>Nix Media</h1>
        <fieldset>
          <p>You have received an invite from {this.state.inviterName}</p>
          <label htmlFor="name">
            Name
            <input
              type="text"
              name="name"
              placeholder="name"
              required
              value={this.state.name}
              onChange={this.saveToState}
            />
          </label>
          <br />
          <label htmlFor="email">
            Email
            <input
              type="email"
              name="email"
              placeholder="email"
              required
              value={this.state.email}
              onChange={this.saveToState}
            />
          </label>
          <br />
          <label htmlFor="password">
            Password
            <input
              type="password"
              name="password"
              placeholder="password"
              required
              value={this.state.password}
              onChange={this.saveToState}
            />
          </label>
          <br />
          <p>To accept, please confirm your details and provide a password</p>
          <br />
          <button className="btn btn-success" type="submit">
            Sign Up!
          </button>
        </fieldset>
      </form>
    );
  }
}

export default InviteSignupForm;
