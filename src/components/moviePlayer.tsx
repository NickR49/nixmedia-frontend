import * as React from 'react';
import ReactPlayer from 'react-player';

interface MoviePlayerProps {
  url: string;
}

export default function MoviePlayerProps(props: MoviePlayerProps) {
  console.log(`MoviePlayer.render() - url: ${props.url}`);

  return (
    <div
    // className="player-wrapper"
    >
      <ReactPlayer
        // className="react-player"
        url={props.url}
        playing={true}
        controls={true}
        width="100"
        height="100%"
      />
    </div>
  );
}
