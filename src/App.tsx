import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import Signup from './pages/signup';
import Home from './pages/home';
import Login from './pages/login';
import ProtectedRoute from './components/common/ProtectedRoute';
import Movies from './pages/Movies';

// import "react-toastify/dist/ReactToastify.min.css";

class App extends Component {
  render() {
    return (
      <>
        <ToastContainer position="top-center" hideProgressBar={true} />
        <Switch>
          <Route path="/signup" component={Signup} />
          <Route path="/login" exact component={Login} />
          <ProtectedRoute path="/movies" exact component={Movies} />
          <Route path="/" exact component={Home} />
          <Redirect to="/not-found" />
        </Switch>
      </>
    );
  }
}

export default App;
