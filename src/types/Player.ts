export interface IPlayer {
  id: string;
  name: string;
  score: number;
}

export default class Player implements IPlayer {
  id: string;
  name: string;
  score: number;

  constructor() {
    this.id = '';
    this.name = '';
    this.score = 0;
  }
}
