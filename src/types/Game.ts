import { Tile, Player, RackSquare, BoardSquare } from './types';

export interface IGame {
  gameId: string;
  playerTiles: Tile[];
  players: Player[];
  boardSquares: BoardSquare[][];
  rackSquares: RackSquare[];
}

export default class Game implements IGame {
  gameId: string;
  playerTiles: Tile[];
  players: Player[];
  boardSquares: BoardSquare[][];
  rackSquares: RackSquare[];

  constructor() {
    this.gameId = '';
    this.playerTiles = [];
    this.players = [];
    this.boardSquares = [];
    this.rackSquares = [];
  }
}
