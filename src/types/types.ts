export interface Game {
  gameId: string;
  playerTiles: Tile[];
  players: Player[];
  boardSquares: BoardSquare[][];
  rackSquares: RackSquare[];
}

export interface Player {
  id: string;
  name: string;
  score: number;
}

export interface Tile {
  tileId: number;
  letter: string;
  value: number;
  state?: TileState;
  container?: any;
}

export interface BoardTile extends Tile {
  x: number;
  y: number;
}

export interface BoardWord {
  startCoord: Coord;
  endCoord: Coord;
  word: string;
}

export interface Coord {
  x: number;
  y: number;
}

export interface Square {
  id: number;
  tile: Tile | null;
  inRack?: boolean;
  onBoard?: boolean;
}

export interface RackSquare extends Square {
  text?: string;
}

export interface BoardSquare extends Square {
  x: number;
  y: number;
  text?: string;
}

export enum Orientation {
  HORIZONTAL = 0,
  VERTICAL = 1,
}

export enum TileState {
  TILE_IN_BAG = 0,
  TILE_IN_RACK = 1,
  TILE_LOOSE_ON_BOARD = 2,
  TILE_FIXED_ON_BOARD = 3,
}
