let token = '';

export function setJwt(jwt: string) {
  // axios.defaults.headers.common["x-auth-token"] = jwt;
  token = jwt;
}

export function getJwt() {
  return token;
}

export default {
  setJwt,
  getJwt,
};
