import jwtDecode from 'jwt-decode';

const tokenKey = 'token';

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    if (jwt === null) {
      return null;
    }
    const result = jwtDecode(jwt);
    console.log('getCurrentUser: ', result);
    return result;
  } catch (ex) {
    return null;
  }
}

export default { getCurrentUser };
