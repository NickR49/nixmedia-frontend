import { RackSquare, BoardSquare } from '../types/types';

export function setRack() {
  var rackSquares: RackSquare[] = Array(7);
  for (let x = 0; x < 7; x++) {
    rackSquares[x] = { id: x, tile: null, inRack: true };
  }
  return rackSquares;
}

export function setBoardLayout() {
  var boardSquares: BoardSquare[][] = Array(15);

  let index = 0;
  for (let y = 0; y < 15; y++) {
    boardSquares[y] = Array(15);
    for (let x = 0; x < 15; x++) {
      boardSquares[y][x] = {
        id: index++,
        text: '',
        tile: null,
        y,
        x,
        onBoard: true,
      };
    }
  }

  // boardSquares[1][1].tile = { id: 204, letter: "Q", value: 10, y: 1, x: 1 };
  // boardSquares[1][2].tile = { id: 205, letter: "W", value: 4, y: 1, x: 2 };

  boardSquares[0][0].text = 'TW';
  boardSquares[0][7].text = 'TW';
  boardSquares[0][14].text = 'TW';
  boardSquares[7][0].text = 'TW';
  boardSquares[7][14].text = 'TW';
  boardSquares[14][0].text = 'TW';
  boardSquares[14][7].text = 'TW';
  boardSquares[14][14].text = 'TW';
  boardSquares[1][1].text = 'DW';
  boardSquares[1][13].text = 'DW';
  boardSquares[2][2].text = 'DW';
  boardSquares[2][12].text = 'DW';
  boardSquares[3][3].text = 'DW';
  boardSquares[3][11].text = 'DW';
  boardSquares[4][4].text = 'DW';
  boardSquares[4][10].text = 'DW';
  boardSquares[10][4].text = 'DW';
  boardSquares[10][10].text = 'DW';
  boardSquares[11][3].text = 'DW';
  boardSquares[11][11].text = 'DW';
  boardSquares[12][2].text = 'DW';
  boardSquares[12][12].text = 'DW';
  boardSquares[13][1].text = 'DW';
  boardSquares[13][13].text = 'DW';
  boardSquares[1][5].text = 'TL';
  boardSquares[1][9].text = 'TL';
  boardSquares[5][1].text = 'TL';
  boardSquares[5][5].text = 'TL';
  boardSquares[5][9].text = 'TL';
  boardSquares[5][13].text = 'TL';
  boardSquares[9][1].text = 'TL';
  boardSquares[9][5].text = 'TL';
  boardSquares[9][9].text = 'TL';
  boardSquares[9][13].text = 'TL';
  boardSquares[13][5].text = 'TL';
  boardSquares[13][9].text = 'TL';
  boardSquares[0][3].text = 'DL';
  boardSquares[0][11].text = 'DL';
  boardSquares[2][6].text = 'DL';
  boardSquares[2][8].text = 'DL';
  boardSquares[3][0].text = 'DL';
  boardSquares[3][7].text = 'DL';
  boardSquares[3][13].text = 'DL';
  boardSquares[6][2].text = 'DL';
  boardSquares[6][6].text = 'DL';
  boardSquares[6][8].text = 'DL';
  boardSquares[6][12].text = 'DL';
  boardSquares[7][3].text = 'DL';
  boardSquares[7][11].text = 'DL';
  boardSquares[8][2].text = 'DL';
  boardSquares[8][6].text = 'DL';
  boardSquares[8][8].text = 'DL';
  boardSquares[8][12].text = 'DL';
  boardSquares[11][0].text = 'DL';
  boardSquares[11][7].text = 'DL';
  boardSquares[11][14].text = 'DL';
  boardSquares[12][6].text = 'DL';
  boardSquares[12][8].text = 'DL';
  boardSquares[14][3].text = 'DL';
  boardSquares[14][11].text = 'DL';
  // boardSquares[7][7].text = "*";
  // boardSquares[7][7].text = "\uD83D\uDFD0";
  // boardSquares[7][7].text = "&#x1f7d0";
  boardSquares[7][7].text = '\u2739';

  return boardSquares;
}
