# Nix Media

## Frontend

### Technologies

 - TypeScript
 - Apollo Client
 - ReactJS
 - Docker
 
### Build

The frontend commits trigger BitBucket pipelines which builds the app, builds a Docker image and uploads it to GCR. 

### Known Bugs

1. Dragging the movie carousel can cause a movie to inadvertantly start playing.

2. Login is not working for mobile devices - network issue.

3. Building the Docker image in BitBucket pipelines causes the app to have a network error when running.
For now follow this manual build process -

 - `npm run build`
 - `docker build -t tfg/nixmedia-frontend-prod .`
 - `docker tag tfg/nixmedia-frontend-prod:latest asia.gcr.io/nixmedia/frontend`
 - `docker push asia.gcr.io/nixmedia/frontend`
