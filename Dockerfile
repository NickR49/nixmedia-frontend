FROM node:12.8-alpine

ENV NPM_CONFIG_LOGLEVEL warn

RUN npm install -g serve --unsafe-perm
EXPOSE 3000

COPY ./build ./build

ENTRYPOINT ["serve", "-s", "/build", "-l", "3000"]
